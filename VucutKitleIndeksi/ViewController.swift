//
//  ViewController.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 8.09.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit
import  Firebase

class ViewController: UIViewController {
    var ref = Database.database().reference()
    let user = Auth.auth().currentUser
   
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var TfName: UITextField!
    @IBOutlet weak var TfBoy: UITextField!
    @IBOutlet weak var TfKilo: UITextField!
    @IBOutlet weak var lbSonuc: UILabel!
    @IBOutlet weak var lbDurum: UILabel!
    @IBOutlet weak var switch1: UISwitch!
   
    @IBAction func geriGit(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let anaSayfa = storyBoard.instantiateViewController(withIdentifier: "rootPageVC")
        self.present(anaSayfa, animated: true, completion: nil)
    }
    func sonuc(kilo:Float,boy:Float)->Int{
        let vki = Int((kilo/(boy*boy)*10000))
        return vki
    }
    @IBAction func btnSonucuGoster(_ sender: Any) {
        // Boy ve kilo boş geçilemeceğini kontrol ediyorum.
        if(TfBoy.text=="" || TfKilo.text=="")
        {
            let uyari = UIAlertController(title: "title", message: "bir bir alert", preferredStyle:UIAlertControllerStyle.alert)
            uyari.addAction(UIAlertAction(title: "Tamam", style: UIAlertActionStyle.default, handler: { (action) in}))
                uyari.dismiss(animated: true, completion:nil)
            self.present(uyari, animated: true, completion: nil)
        }
        else
        {   // Kadınlar için.
            if(switch1.isOn)
            {
                let boy = Float(self.TfBoy.text!)
                let kilo = Float(self.TfKilo.text!)
                let vki = self.sonuc(kilo: kilo!, boy: boy!)
                lbSonuc.text = String(vki)
                if(vki<18)                   {lbDurum.text = "Zayıf" }
                else if(vki>=17   || vki<=23){lbDurum.text = "Normal"}
                else if(vki>=24   || vki<=28){lbDurum.text = "Kilolu"}
                else if(vki>=29)             {lbDurum.text = "Obez"  }

            }else
            {// Erkekler için.
            let boy = Float(self.TfBoy.text!)
            let kilo = Float(self.TfKilo.text!)
            let vki = self.sonuc(kilo: kilo!, boy: boy!)
            lbSonuc.text = String(vki)
                if(vki<18)                  {lbDurum.text = "Zayıf" }
                else if(vki>=18 || vki<=24) {lbDurum.text = "Normal"}
                else if(vki>=25 || vki<=29) {lbDurum.text = "Kilolu"}
                else if(vki>=30)            {lbDurum.text = "Obez"  }
            }
        }
        
        let uniqeID = ref.child("veriler").childByAutoId().key
        func unixTimeConvertion(unixTime: Double) -> String {
            let time = NSDate(timeIntervalSince1970: unixTime)
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.locale = Locale.init(identifier: "tr-TR")
            return dateFormatter.string(from: time as Date)
        }

        let date = (NSDate().timeIntervalSince1970)
        let dates = unixTimeConvertion(unixTime: date)

        
        ref.child("veriler").child((user?.uid)!).child(uniqeID).child("Tarih").setValue(dates)
        ref.child("veriler").child((user?.uid)!).child(uniqeID).child("Boy").setValue(TfBoy.text)
        ref.child("veriler").child((user?.uid)!).child(uniqeID).child("Kilo").setValue(TfKilo.text)
        ref.child("veriler").child((user?.uid)!).child(uniqeID).child("Vki").setValue(lbSonuc.text)
        ref.child("veriler").child((user?.uid)!).child(uniqeID).child("Durum").setValue(lbDurum.text)
        
}
  
    func unixTimeConvertion(unixTime: Double) -> String {
        let time = NSDate(timeIntervalSince1970: unixTime)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: NSLocale.system.identifier) as Locale!
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter.string(from: time as Date)
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Kullanıcın Adını  almam için users tablosunda uid ulaşmam gerek uid altında adSoyadı almam gerek
        let uid = Auth.auth().currentUser?.uid
        self.ref.child("users").child(uid!).child("adSoyad").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let isim = snapshot.value as! String
            self.lbName.text = ("Hoşgeldin \(isim)")
            print("Kullanıcı İsmi \(isim)")
        })
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

