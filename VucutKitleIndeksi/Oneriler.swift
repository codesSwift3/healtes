//
//  Oneriler.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 10.09.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit
import Firebase

class Oneriler: UIViewController {

    @IBOutlet weak var lbgelenAd: UILabel!
    @IBOutlet weak var lbGelenSonuc: UILabel!
    @IBOutlet weak var oneri2: UITextView!
    @IBOutlet weak var oneri1: UITextView!
    @IBOutlet weak var lbSonDurum: UILabel!
    
    let user = Auth.auth().currentUser
    let ref = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()

            self.ref.child("veriler/\(self.user!.uid)").observe(.childAdded, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                let durum = value?["Durum"] as! String
          
                
                self.lbSonDurum.text = durum
                self.durumf(gelenDurum: durum )
                
            })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func detaylaraGit(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let kayitlar = storyBoard.instantiateViewController(withIdentifier: "kayitlar") as! Detaylar
        self.present(kayitlar, animated: true, completion: nil)
    }
    
    @IBAction func anaSayfayaGit(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let anaSayfa = storyBoard.instantiateViewController(withIdentifier: "anaSayfa") as! ViewController
        self.present(anaSayfa, animated: true, completion: nil)
    }

    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func durumf(gelenDurum : String){
        
        if(gelenDurum == "Zayıf")
        {
            oneri1.text = "Elinizin altında kuru meyve bulundurun: Kuru meyvelerin kalori içeriği yüksek olmakla birlikte posa, vitamin ve mineral içeriği olarak da oldukça zengindir.Sütlü tatlı tüketmeyi ihmal etmeyin: Sütlü tatlılar, protein ve kalsiyum alımınıza katkı sağlarken, aynı zamanda kilo almanız için ihtiyacınız olan enerji için iyi bir destekleyicidir.  "
            oneri2.text = "Sigara içiyorsanız bırakın: Sigaranın iştahı kapattığı ve sigaradaki nikotin maddesinin metabolizmayı hızlandırıcı etkisi olduğu bilinmektedir.Sıvı tüketiminizi kontrol altına alın: Günlük ortalama 1,5-2 litreden fazla su tüketmemeye çalışın. Gereğinden fazla su tüketimi midede doygunluk hissini arttırır ve farkında olmadan ihtiyacınızdan az besin tüketirsiniz. "
        }
        else if(gelenDurum == "Normal")
        {
            oneri1.text = "Formunuzu korumanın sırrı, protein ve karbonhidrat dengesinde yatar. Alınması gereken toplam kalori miktarı içinde; karbonhidrat ve protein dengeli olduğu sürece kilo almazsınız. Bu da proteinlerle karbonhidratların gün içindeki dağılımını eşit tutmaya dikkat etmek anlamına gelmektedir. ."
            oneri2.text = "Gün içinde hiç protein almayıp, sadece karbonhidrat ile beslenirseniz kilo alır, proteini arttırıp karbonhidratı azaltırsanız, zayıflamaya devam edersiniz."
        }
        else if(gelenDurum == "Kilolu")
        {
            oneri1.text = " İnanmak, başarmanın yarısıdır.Zayıflayanlardan hiçbir eksiğinin olmadığını bilmelisin; bu fazlalıklardan kurtulmak adına kesin karar vererek zayıflama sürecini başlamalısın. Önce niyet, sonra diyet :) Bir egzersiz programına başlamalı,yemek süresi uzatılmalı böylece tokluk hissini ortalama olarak 20. dakikada oluşturmalısın. Yenilenlerden keyif ve tat alabilmek adına besinler iyice çiğnenmeli, küçük lokmalar halinde yutmanı öneririm."
            
            oneri2.text = "Kurubaklagiller (kuru fasulye, nohut, mercimek, kuru barbunya), kepekli tahıllar (esmer ekmek, yulaf, bulgur, kepekli pirinç / makarna / erişte / mantı / un), sebze ve meyveler içerdikleri lifler sayesinde midede hacim sağlayarak uzun süre seni tok tutar ve diyete uyumunu artırırlar. Ayrıca şeker, kolesterol ve tansiyonu istenilen seviyelerde tutmaya yardımcı olmaktadır. Tuvalete çıkma sayısını ve miktarını artırarak kabızlığı önlemekte, kalın bağırsak kanserinden koruyucu etkiler içermektedir. Lif alımını artırmak adına soyulmadan yenilebilen sebze ve meyveleri kabukları ile birlikte tüketmekte yarar vardır."
            
        }
        else if(gelenDurum == "Obez")
        {
            oneri1.text = "Düzenli egzersiz ve uykunun da zayıflama üzerinde olumlu etkileri var. Karanlık bir odada 7-8 saatlik düzenli gece uykusu, vücudumuzdaki tüm faaliyetleri düzene koyan bir dinlenme sürecidir. Metabolizmayı hızlandırmak için haftada en az 3 kez ve 30-45 dakika süreyle düzenli fiziksel aktivite yapmayı ihmal etmemelisin veya bir egzersiz programına kayıt olmalısın."
            oneri2.text = "Düzenli aralıklarla vücutta oluşan zararlı maddelerin atılması ve bağırsak sağlığı için günde en az 2 litre su tüketmeye özen göstermelisin.Öğün atlamadan porsiyon kontrolü için yemeklerinizi mümkün olduğunca küçük tabaklarda tüketmeye özen göstermelisin. Basit karbonhidratlı ve şekerli besinler yerine, kan şekerini düzenleyen ve stres seviyesini azaltmaya yardımcı olan doğal ve işlenmemiş gıdaları, tam tahıllı ekmek, makarnalar, bulgur, kuru baklagiller, taze sebze ve meyve gibi lifli besinleri tüketmelsin."
        }
        

    }
}
