//
//  vkiPage.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 21.10.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit

class vkiPage: UIViewController {
    @IBAction func anaSayfayaGit(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let anaSayfa = storyBoard.instantiateViewController(withIdentifier: "anaSayfa")
        self.present(anaSayfa, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
