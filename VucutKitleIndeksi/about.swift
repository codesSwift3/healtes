//
//  about.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 28.09.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit
import Firebase

class about: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var tfAdSoyad: UITextField!
    @IBOutlet weak var tfKullaniciAdi: UITextField!
    @IBOutlet weak var tfHedefKilo: UITextField!
    @IBOutlet weak var tfMotiveCumle: UITextField!
    
    let ref = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfAdSoyad.delegate      = self
        tfKullaniciAdi.delegate = self
    }
    @IBAction func uygulamayaGit(_ sender: Any) {
        let userId = Auth.auth().currentUser?.uid
        print(userId!)
        // Kullanıcıadlarını tutmak için alan oluşturuyorum
        ref.child("kullaniciAdi").child(self.tfKullaniciAdi.text!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if(!snapshot.exists())
            {
                self.ref.child("users").child(userId!).child("kullaniciAdi").setValue(self.tfKullaniciAdi.text!.lowercased())
                self.ref.child("users").child(userId!).child("adSoyad").setValue(self.tfAdSoyad.text)
                self.ref.child("users").child(userId!).child("motiveCumle").setValue(self.tfMotiveCumle.text)
                self.ref.child("users").child(userId!).child("hedefKilo").setValue(self.tfHedefKilo.text)
                
                // id ile birbirine bağlıyorum.
                self.ref.child("kullaniciAdi").child(self.tfKullaniciAdi.text!.lowercased()).setValue(userId!)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let uygulamayaGit = storyBoard.instantiateViewController(withIdentifier: "anaSayfa")
                self.present(uygulamayaGit, animated: true, completion: nil)

            }
                
            else
            {
                print("bu kullanıcı adı daha önce alınmıış")
              
            }
            
            
            // ...
        })
        {
            (error) in
            print(error.localizedDescription)
        }

       
        
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
  }
