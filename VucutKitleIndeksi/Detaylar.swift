//
//  Detaylar.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 10.09.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit
import Firebase
class Detaylar: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var tvMotiveCumle: UITextView!
    
    let ref = Database.database().reference()
    let user = Auth.auth().currentUser?.uid
    var userInfo = [String]()
    var tarihler = [String]()
    var kilolar = [String]()
    var vkiler = [String]()
    var hedefKilolar = [String]()
    var IntKilolar = [Int]()
    var IntHedefKilolar = [Int]()
    var durumlar = [String]()
    
    
    @IBOutlet weak var verilerTableView: UITableView!

    override func viewDidLoad() {
        
        ref.child("users").child(user!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let userValue = snapshot.value as? NSDictionary
            let isim         = userValue?["adSoyad"] as! String
            let kullaniciAdi = userValue?["kullaniciAdi"] as! String
            let motiveCumle  = userValue?["motiveCumle"] as! String
            let hedefKilo    = userValue?["hedefKilo"]   as! String
            
            
            self.tvMotiveCumle.text = motiveCumle
            print("Kullanıcı Adi :\(kullaniciAdi)")
            self.userInfo.append(isim)
            self.userInfo.append(kullaniciAdi)
            self.hedefKilolar.append(hedefKilo)
            
            
            
            self.ref.child("veriler/\(self.user!)").observe(.childAdded, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                let kilo  = value?["Kilo"] as! String
                let tarih = value?["Tarih"] as! String
                let vki   = value?["Vki"] as! String
                let boy   = value?["Boy"] as! String
                let durum = value?["Durum"] as! String
                
                let IntKilo:Int = Int(kilo)!
                let IntHedefKilo:Int = Int(hedefKilo)!
                self.IntKilolar.append(IntKilo)
                self.IntHedefKilolar.append(IntHedefKilo)
                
                self.kilolar.append(kilo)
                self.vkiler.append(vki)
                self.tarihler.append(tarih)
                self.durumlar.append(durum)
                
               self.verilerTableView.insertRows(at: [IndexPath(row:self.tarihler.count-1,section:0)], with: UITableViewRowAnimation.automatic)
            })
        }) { (error) in
            
            print(error.localizedDescription)
        }
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return tarihler.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! Veriler
        let kiloCount = kilolar.count
        let vkiCount  = vkiler.count
        let tarihCount = tarihler.count
        let hedefKilolarCount = hedefKilolar.count
        let durumlarCount = durumlar.count
        let IntHedefKilolarCount = IntHedefKilolar.count
        let IntKilolarCount = IntKilolar.count
        cell.lbKilo.text = kilolar[kiloCount-1]
        cell.lbVki.text = vkiler[vkiCount-1]
        cell.lbTarih.text = tarihler[tarihCount-1]
        cell.lbHedefKilo.text = hedefKilolar[hedefKilolarCount-1]
        cell.lbDurum.text = durumlar[durumlarCount-1]
        cell.hedefKiloYorum(kilo: IntKilolar[IntKilolarCount-1], hedefKilo:IntHedefKilolar[IntHedefKilolarCount-1])
                
        return cell
    }
    

    @IBAction func geriGit(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let oneriler = storyBoard.instantiateViewController(withIdentifier: "oneriler") as! Oneriler
        self.present(oneriler, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
