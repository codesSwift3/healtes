//
//  RootPageViewController.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 21.10.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit

class RootPageViewController: UIPageViewController, UIPageViewControllerDataSource  {
    
    lazy var viewControllerList:[UIViewController ] = {
        
        let sb = UIStoryboard(name: "Main", bundle:nil)
        
        let vc1 = sb.instantiateViewController(withIdentifier: "vkiVC")
        let vc2 = sb.instantiateViewController(withIdentifier: "vucutOlculeriVC")
        
        return [vc1,vc2]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:  viewController) else {return nil}
        let previusIndex = vcIndex - 1
        guard previusIndex >= 0 else {return nil}
        
        guard viewControllerList.count > previusIndex else {return nil}
        return viewControllerList[previusIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:  viewController) else {return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count != nextIndex else {return nil}
        
        guard viewControllerList.count > nextIndex else {return nil}
        
        return viewControllerList[nextIndex]
        
    }
}

