//
//  Veriler.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 1.10.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit

class Veriler: UITableViewCell {

    @IBOutlet weak var lbKilo: UILabel!
    @IBOutlet weak var lbVki: UILabel!
    @IBOutlet weak var lbTarih: UILabel!
    @IBOutlet weak var lbHedefKilo: UILabel!
    @IBOutlet weak var lbDurum: UILabel!
    @IBOutlet weak var lbHedefKiloDurumu: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func hedefKiloYorum(kilo:Int,hedefKilo:Int){
        if (kilo > hedefKilo){
            lbHedefKiloDurumu.text = "\(kilo-hedefKilo) kg fazlanız var :)"
        }
        else if(hedefKilo > kilo){
             lbHedefKiloDurumu.text = "\(hedefKilo-kilo) kg eksiğin var :)"
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
