//
//  SignInUp.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 14.09.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit
import Firebase

class SignInUp: UIViewController, UITextFieldDelegate{
    var ref = Database.database().reference()
    @IBOutlet weak var lbHata: UILabel!
    @IBOutlet weak var titreyenBtnKaydol: animasyon!
    @IBOutlet weak var tfMail: UITextField!
    @IBOutlet weak var tfSifre: UITextField!
    @IBOutlet weak var tfSifreTekrar: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfMail.delegate         = self
        tfSifre.delegate        = self
        tfSifreTekrar.delegate  = self
        titreyenBtnKaydol.layer.cornerRadius = 12
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func Kaydol(_ sender: Any) {
        if(tfSifre.text != tfSifreTekrar.text)
        {
            self.lbHata.text = "Şifreler Uyuşmuyor"
            self.titreyenBtnKaydol.titret()
        }
        else if(!(tfMail.text?.isEmpty)! && !(tfSifre.text?.isEmpty)! && !(tfSifreTekrar.text?.isEmpty)!)
        {
            Auth.auth().createUser(withEmail: tfMail.text!, password: tfSifre.text!, completion: { (user, error) in
                
                if (error != nil){
                    if let hataKodu = AuthErrorCode(rawValue: error!._code) {
                        
                        switch hataKodu {
                        case .invalidEmail:
                            self.lbHata.text = "Böyle Bir Mail Adresi Yok"
                            self.titreyenBtnKaydol.titret()
                        case .emailAlreadyInUse:
                            self.lbHata.text = "Bu Mail Adresi Kayıtlı"
                            self.titreyenBtnKaydol.titret()
                        case .weakPassword:
                            self.lbHata.text = "Güçsüz Şifre. Tekrar Deneyin"
                            self.titreyenBtnKaydol.titret()
                        case .networkError:
                            self.lbHata.text = "İnternet Bağlantı Hatası"
                            self.titreyenBtnKaydol.titret()
                        default:
                            self.lbHata.text = "Bilinmeyen Bir Hata Saptandı."
                            self.titreyenBtnKaydol.titret()
                        }
                        
                    }
                }
                
                else {

                    self.ref.child("users").child(user!.uid).setValue(["email": self.tfMail.text,"sifre":self.tfSifre.text])
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let signIn = storyBoard.instantiateViewController(withIdentifier: "signIn") as! SignIn
                    self.present(signIn, animated: true, completion: nil)

                    print("KAYIT BAŞARIYLA TAMAMLANDI")
                    self.tfMail.text         = ""
                    self.tfSifre.text        = ""
                    self.tfSifreTekrar.text  = ""
                    
                }
                
            })
        }
        
    }
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
