//
//  SignIn.swift
//  VucutKitleIndeksi
//
//  Created by Yakup Caglan on 14.09.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import UIKit
import Firebase

class SignIn: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var lbHata: UILabel!
    @IBOutlet weak var btnDene: UIButton!
    @IBOutlet weak var btnKaydol: UIButton!
    @IBOutlet weak var titreyenBtnGiris: animasyon!
    @IBOutlet weak var tfMail: UITextField!
    @IBOutlet weak var tfSifre: UITextField!
    var ref = Database.database().reference()

    override func viewDidLoad() {
        super.viewDidLoad()
        tfMail.delegate = self
        tfSifre.delegate = self
        
        titreyenBtnGiris.layer.cornerRadius = 12
        btnKaydol.layer.cornerRadius = 12
        btnDene.layer.cornerRadius = 12

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func Giris(_ sender: Any) {
        if(!(tfMail.text?.isEmpty)! && !(tfSifre.text?.isEmpty)!){
            // Bir mail mi kullanıcı adı mı belirliyorum.
            if isValidEmail(testStr: tfMail.text!) == false {
                
                // Kullanıcı mentionName ile giriş yapmışsa, böyle bir kullanıcı adının olup olmadığını kontrol ediyoruz.
                
                self.ref.child("kullaniciAdi").child(tfMail.text!).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if snapshot.exists() == true {
        
                        // Kullanıcı adı doğruysa, kullanıcı adının ait olduğu UserID karşılığını çekiyoruz.
                        
                        let uid = snapshot.value as! String
                        
                        print(uid)
                        // Her ihtimale karşı UserID'nin çekilip çekilmediğini kontrol ediyoruz.
                        
                        if uid.isEmpty != true {
                            // UserID başarılı bir şekilde çekilmişse users tablomuzda giriş yapmaya çalışan kullanıcnın emailini alıyoruz.
                            self.ref.child("users").child(uid).child("email").observeSingleEvent(of: .value, with: { (snapshot) in
                                
                                let email = snapshot.value as! String
                                print(email)
                                
                                print(snapshot)
                                // Yine her ihtimale karşı Emailin çekilip çekilmediğini kontrol ediyoruz.
                                if email.isEmpty == true {
                                    print("birşeyler yanlış")
                                }
                                else {
                                    // Son olarak çekilen email ile kullanıcıya giriş yaptırıyoruz.
                                    
                                    self.emailleGiris(email: email, sifre: self.tfSifre.text!)
                                    UserDefaults.standard.set(email, forKey: "email")
                                    UserDefaults.standard.set(self.tfSifre.text, forKey: "sifre")
                                }
                            })
                        }
                    }
                    else
                    {
                        // Öyle bir kullanıcı adı yoksa, güvenlik sebebiyle uyarıyı aşağıdaki gibi gösteriyoruz..
                        print("böyle bir kullanıcı adı yok")
                    }
                    
                })}
                
            else
            {
                emailleGiris(email: tfMail.text!, sifre: tfSifre.text!)
                UserDefaults.standard.set(self.tfMail.text, forKey: "email")
                UserDefaults.standard.set(self.tfSifre.text, forKey: "sifre")
            }
            
        }
        else{
            print("Boş alan bırakmayınız")
        }

    }

    func emailleGiris(email: String, sifre: String){
        
        Auth.auth().signIn(withEmail: email, password: sifre, completion: { (user, error) in
            if(error != nil){
                if let hataKodu = AuthErrorCode(rawValue: error!._code) {
                    switch hataKodu{
                    case .invalidEmail:
                        self.lbHata.text = "Mail adresi veya Şifre Hatalı"
                        self.titreyenBtnGiris.titret()
                    case .wrongPassword:
                        self.lbHata.text = "Mail adresi veya Şifre Hatalı"
                        self.titreyenBtnGiris.titret()
                    case .networkError:
                        self.lbHata.text = "İnternet Bağlantı Hatası"
                        self.titreyenBtnGiris.titret()
                    default:
                        self.lbHata.text = "Bilinmeyen Bir hata ile karşılaşıldı"
                        self.titreyenBtnGiris.titret()
                    }
                }
            }
            else{
                // kullanıcının id'sini alıyorum.
                let uid = Auth.auth().currentUser?.uid
                // kullanıcı id'si ile gelen kullanıcı adının "kullaniciAdi" keyinde karşlığı varmı yok mu anlamak için
                // observeSingleEvent fonksiyonuyla dinliyorum. Ona göre sayfalar arası yönlendirme yapıyorum.
                self.ref.child("users").child(uid!).observeSingleEvent(of: .value, with: { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    let kullaniciAdi = value?["kullaniciAdi"] as? String ?? ""
                    print(kullaniciAdi)
                    
                    if kullaniciAdi == "" {
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let about = storyBoard.instantiateViewController(withIdentifier: "about") as! about
                        self.present(about, animated: true, completion: nil)
                    }
                    else {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let anaSayfa = storyBoard.instantiateViewController(withIdentifier: "rootPageVC")
                        self.present(anaSayfa, animated: true, completion: nil)
                    }
                })
                
            
            
            }
        })
    }
    // girilen textin mail olup olmadığını kontrol etmek amaçlı.
    func isValidEmail(testStr:String) -> Bool {
        
        print("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
        
    }
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
